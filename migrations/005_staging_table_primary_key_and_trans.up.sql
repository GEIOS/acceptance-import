alter table gos_tdi_interface add column id bigserial;
alter table gos_tdi_interface add primary key (id);
alter table gos_tdi_interface add column processing_transaction varchar(64);
