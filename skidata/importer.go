package skidata

import (
	"acceptance-import/db"
	"acceptance-import/shared"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/lib/pq"
	"github.com/mitchellh/mapstructure"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var (
	ConfigDemoSystem = Config{
		"http://crs.skidata.com/%s/TicketDataInterface.svc/json/GetTicketEventData",
		[]string{"Skidata.TDI.DEMO"},
		"a2f8879e-ee2a-407b-a39b-6eb8b1da0d2e",
		"TEST",
		"demo",
	}
	ConfigTempSystem = Config{
		"https://tdi.webhost.skidata.com/%s/TicketDataInterface.svc/json/GetTicketEventData",
		[]string{"TDI.CHE"},
		"9690a562-35aa-45b4-b338-57a3229041ea",
		"HTG",
		"temp",
	}
	ConfigLocalhostProdTunnel = Config{
		"http://localhost:8888/%s/TicketDataInterface.svc/json/GetTicketEventData",
		[]string{"Skidata.TDI"},
		"80f98c4c-a8d6-4bce-af48-06f3ddc65e9c",
		"HTG",
		"prod",
	}
	ConfigEnvServerGatewayMachine = Config{
		os.ExpandEnv("http://$SKIDATA_HOST:$SKIDATA_PORT/%s/TicketDataInterface.svc/json/GetTicketEventData"),
		[]string{"Skidata.TDI"},
		"80f98c4c-a8d6-4bce-af48-06f3ddc65e9c",
		"HTG",
		"prod",
	}
	ConfigConsolidatedSkidataSystem = Config{
		os.ExpandEnv("https://tdi.webhost.skidata.com/%s/TicketDataInterface.svc/json/GetTicketEventData"),
		[]string{"TDI.CHE"},
		"9690a562-35aa-45b4-b338-57a3229041ea",
		"HTG",
		"prod",
	}
	timestampRegexp = regexp.MustCompile(`\/Date\(([0-9]+)\+([0-9]{2})([0-9]{2})\)\/`)
)

type (
	Config struct {
		Url           string
		UrlStringArgs []string
		ApiKey        string
		Tenant        string
		ConfigKey     string
	}

	ticket struct {
		ItemId          string
		TicketEventType float64
		ProductId       string
		AreaFromName    string
		LocationName    string
		PermissionId    string
		DataCarrierId   string
		Timestamp       string
		ConvertedTime   time.Time
		OrigJsonData    string
	}
)

const (
	httpConcurrency = 1
	dbConcurrency   = 2
)

func (config Config) urlArgs() []interface{} {
	return convertSlice(config.UrlStringArgs)
}

func convertSlice(strings []string) []interface{} {
	i := make([]interface{}, len(strings))
	for idx, val := range strings {
		i[idx] = val
	}
	return i
}

func ImportData(config Config, date shared.TdiDate) []error {
	client := http.Client{}
	url := fmt.Sprintf(config.Url, config.urlArgs()...)
	log.Printf("Importing from TDI interface for date: %s", date.Format(time.RFC822Z))
	bytes, err := executeRequest(client, config, url, date, 0, fmt.Sprintf("page %d", 0))
	if err != nil {
		return []error{err}
	}
	firstData, err := extractData(bytes)
	if len(firstData) == 0 {
		return []error{errors.New("received empty response")}
	}
	if err != nil {
		return []error{err}
	}
	fillChan := func(ch chan<- bool) {
		for i := 0; i < cap(ch); i++ {
			ch <- true
		}
	}
	remainingPages := getPageCount(firstData) - 1
	log.Printf("Total page count is %d", remainingPages+1)
	httpSem := make(chan bool, httpConcurrency)
	dbSem := make(chan bool, dbConcurrency)
	errorChan := make(chan error, remainingPages)
	// write to db
	req := "page 0"
	tickets, err := getRows(firstData, req)
	if err != nil {
		return []error{err}
	}

	writeDb := func(tickets []ticket, request string) {
		defer func() {
			<-dbSem
		}()
		_, errors := writeTickets(config, tickets, request)
		if errors != nil {
			log.Printf("Error while writing tickets for request %s: %v", request, errors)
		}
	}
	dbSem <- true
	go writeDb(tickets, req)
	// fetch page
	fetchPage := func(page int, pageDesc string) {
		defer func() { <-httpSem }()
		bytes, err := executeRequest(client, config, url, date, page, pageDesc)
		if err != nil {
			log.Println(err)
			errorChan <- err
			return
		}
		data, err := extractData(bytes)
		if err != nil {
			log.Println(err)
			errorChan <- err
			return
		}
		tickets, err := getRows(data, pageDesc)
		if err != nil {
			errorChan <- err
			return
		}
		errorChan <- nil
		dbSem <- true
		go writeDb(tickets, pageDesc)
	}
	for i := 0; i < remainingPages; i++ {
		httpSem <- true
		go fetchPage(i, fmt.Sprintf("page %d of %d", i+1, remainingPages))
	}
	// Wait for http stuff to finish
	fillChan(httpSem)
	// Wait for db stuff to finish
	fillChan(dbSem)
	close(errorChan)
	// Everything finisihed, did we have an error?
	errors := make([]error, 0)
	for error := range errorChan {
		if error != nil {
			errors = append(errors, error)
		}
	}
	if len(errors) > 0 {
		return errors
	}
	return nil
}

func extractData(bytes []byte) (map[string]interface{}, error) {
	data := make(map[string]interface{})
	error := json.Unmarshal(bytes, &data)
	if error != nil {
		return nil, error
	}
	return data, nil

}

func executeRequest(client http.Client, config Config, url string, date shared.TdiDate, pageNumber int, pageDesc string) ([]byte, error) {
	args := fmt.Sprintf(`{"Page": %d, "Date": "%s"}`, pageNumber, date.ToTdiDateString())
	req, err := http.NewRequest("POST", url, strings.NewReader(args))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("SD-TII-APIKEY", config.ApiKey)
	log.Printf("Downloading %s, using url: %s", pageDesc, url)
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	bytes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	log.Printf("Downloaded %s", pageDesc)
	return bytes, nil
}

func getPageCount(data map[string]interface{}) int {
	return int(data["TicketEventDataSearchResult"].(map[string]interface{})["TotalNumberOfPages"].(float64))
}

func getRows(data map[string]interface{}, request string) ([]ticket, error) {
	searchResult := data["TicketEventDataSearchResult"]
	if searchResult == nil {
		log.Printf("No search result in response %s. Will assume this is an error.", request)
		log.Println(data)
		return nil, errors.New(fmt.Sprintf("No search result in response: %s", request))

	}
	m := searchResult.(map[string]interface{})["Data"].([]interface{})
	tickets := make([]ticket, 0, len(m))
	for _, t := range m {
		ticket := ticket{}
		mapstructure.Decode(t, &ticket)
		if ticket.TicketEventType != 3 {
			continue
		}
		ticket.convertTimes()
		buffer := &bytes.Buffer{}
		encoder := json.NewEncoder(buffer)
		encoder.Encode(t)
		ticket.OrigJsonData = buffer.String()
		tickets = append(tickets, ticket)
	}
	return tickets, nil
}

func writeTickets(config Config, tickets []ticket, writeRequest string) (int, []error) {
	log.Printf("Writing %d tickets to database in request %s", len(tickets), writeRequest)
	tx, err := db.Database.Begin()
	if err != nil {
		return 0, shared.ErrorArray(err)
	}
	transactionSuccess := false
	defer db.EndTransaction(tx, &transactionSuccess)
	log.Printf("Started transaction %p for request %s", tx, writeRequest)
	_, err = tx.Exec("create temporary table t_gos_tdi_interface(external_id varchar, tenant varchar, json_content jsonb) on commit drop")
	if err != nil {
		return 0, shared.ErrorArray(err)
	}
	copyStatement, err := tx.Prepare(pq.CopyIn("t_gos_tdi_interface", "external_id", "tenant", "json_content"))
	if err != nil {
		return 0, shared.ErrorArray(err)
	}
	written := 0
	var errors []error
	log.Printf("Starting add to copy statement for request %s", writeRequest)
	for _, t := range tickets {
		if _, err = copyStatement.Exec(t.ItemId, config.Tenant, t.OrigJsonData); err != nil {
			errors = shared.ErrorToArray(fmt.Errorf("%s, item id %s", err.Error(), t.ItemId), errors)
		} else {
			written++
		}
	}
	log.Printf("Finished add to copy statement for request %s", writeRequest)

	log.Printf("Executing copy statement for request %s", writeRequest)
	if _, err = copyStatement.Exec(); err != nil {
		errors = shared.ErrorToArray(err, errors)
	}
	log.Printf("Finished executing copy statement for request %s", writeRequest)
	log.Printf("Executing real insert statement for request %s", writeRequest)
	if _, err = tx.Exec(
		`insert into gos_tdi_interface(external_id, tenant, json_content, import_timestamp) 
				select external_id, tenant, json_content, current_timestamp from t_gos_tdi_interface
				on conflict(external_id) 
				do update set updated_json_content = excluded.json_content, update_timestamp = current_timestamp
`); err != nil {
		errors = shared.ErrorToArray(err, errors)
	}
	log.Printf("Executed real insert statement for request %s", writeRequest)
	transactionSuccess = len(errors) == 0
	return written, shared.ErrorsArrayOrNil(errors)
}

func (ticket *ticket) convertTimes() {
	if ticket.Timestamp != "" {
		t := timestampRegexp.FindStringSubmatch(ticket.Timestamp)
		if t != nil {
			millis, _ := strconv.Atoi(t[1])
			hoursOffset, _ := strconv.Atoi(t[2])
			minutesOffset, _ := strconv.Atoi(t[3])
			ticket.ConvertedTime = time.Unix(int64(millis/1000-hoursOffset*60*60-minutesOffset*60), 0).UTC()
		}
	} else {
		ticket.ConvertedTime = time.Time{}
	}
}
