package skidata

import (
	"acceptance-import/db"
	"acceptance-import/shared"
	"fmt"
	uuid2 "github.com/google/uuid"
	"log"
	"math"
	"time"
)

func MoveTdiToEtl() error {
	log.Print("Will try moving some tdi records to etl processing")
	rows, err := db.Database.Query(`
select count(*) from ms_identity.gos_acceptance_protocol p left join pulse_data.f_acceptances a on a.uuid = p.uuid
where p.attributes->'tdi_json_content' is not null
  and a.uuid is null`)
	if err != nil {
		return err
	}
	defer rows.Close()
	rows.Next()
	var cnt int32
	rows.Scan(&cnt)
	remaining := 5000 - cnt
	if remaining <= 0 {
		log.Printf("There are still %d unprocessed tdi acceptances, will not do anything", cnt)
		return nil
	}
	log.Printf("Will start moving at most %d tdi acceptances to gos_acceptance_protocol", remaining)
	tx, err := db.Database.Begin()
	if err != nil {
		return err
	}
	transactionSuccess := false
	defer db.EndTransaction(tx, &transactionSuccess)
	result, err := db.Database.Exec(fmt.Sprintf(`
with movable_rows as (
delete from ms_identity.%[1]s where
    uuid in (select uuid from ms_identity.%[1]s limit %[2]d)
    returning uuid, card_status, acceptance_timestamp, transaction_id, virtual_card_master_id, stay_uuid,
        service_id, device_id, attributes, tenant, creation, last_modified,
        acceptance_outcome, normalized_card_master_id)
insert into ms_identity.gos_acceptance_protocol
    (uuid, card_status, acceptance_timestamp, transaction_id, virtual_card_master_id, stay_uuid, service_id,
     device_id, attributes, tenant, creation, last_modified, acceptance_outcome, normalized_card_master_id)
select * from movable_rows;
`, "gos_acceptance_protocol_tdi_2019", remaining))
	if err != nil {
		return err
	}
	rumRows, err := result.RowsAffected()
	log.Printf("Moved %d tdi acceptances to gos_acceptance_protocol and wishing the etl job good luck at processing those records.", rumRows)
	transactionSuccess = true
	return nil
}

func ConsolidateAcceptances(from time.Time, until time.Time) (error, time.Time) {
	if until.Before(from) || until.Equal(from) {
		until = from
		log.Printf("Allowed acceptance consolidation date range %s until %s (exclusive) doesn't contain anything, will ignore.", from.Format(time.RFC822Z), until.Format(time.RFC822Z))
		return nil, until
	}
	tx, err := db.Database.Begin()
	if err != nil {
		return err, until
	}
	transactionSuccess := false
	defer db.EndTransaction(tx, &transactionSuccess)
	log.Printf("Will consolidate pre-acceptances between %s and %s (exclusive)", from.Format(time.RFC822Z), until.Format(time.RFC822Z))
	_, err = db.Database.Exec(`
with pre_acceptances as 
    (select * from vw_ranked_tdi_usages 
    where acceptance_timestamp >= $1 and acceptance_timestamp < $2 and import_duplication_rank = 1 and area_rank = 1)
insert into ms_identity.gos_acceptance_protocol_tdi_2019
    (uuid, card_status, acceptance_timestamp, transaction_id, virtual_card_master_id, stay_uuid, 
     service_id, device_id, attributes, tenant, creation, acceptance_outcome)
select uuid_generate_v4(), card_status, acceptance_timestamp, transaction_id, virtual_card_master_id, null, 
       gos_tdi_service_ids.service_id, 'vt.' || pre_acceptances.the_area, attributes, 'HTG', current_timestamp, acceptance_outcome
    from pre_acceptances left join gos_tdi_service_ids on pre_acceptances.the_area = gos_tdi_service_ids.the_area`, from, until)
	if err != nil {
		return err, until
	}
	transactionSuccess = true
	return nil, until
}

func ArchiveProcessedRecords(maxLimit int) error {
	log.Printf("Archiving maximally %d processed tdi records", maxLimit)
	tx, err := db.Database.Begin()
	log.Println("Starting transaction")
	if err != nil {
		return err
	}
	transactionSuccess := false
	defer db.EndTransaction(tx, &transactionSuccess)
	_, err = db.Database.Exec(fmt.Sprintf(`
WITH moved_rows AS (
    DELETE FROM gos_tdi_interface where id in (SELECT id FROM gos_tdi_interface WHERE processing_timestamp is not null LIMIT %d)
        
        RETURNING *
)
INSERT INTO gos_tdi_interface_archive
SELECT * FROM moved_rows`, maxLimit))
	if err != nil {
		return err
	}
	transactionSuccess = true
	return nil
}

func ProcessImportedTickets(maxRows int) (imported, postponed int64, rowErrors []error) {
	log.Printf("Processing maximally %d imported tickets", maxRows)
	tx, err := db.Database.Begin()
	log.Println("Starting transaction")
	if err != nil {
		return 0, 0, shared.ErrorArray(err)
	}
	transactionSuccess := false
	defer db.EndTransaction(tx, &transactionSuccess)
	uuid := uuid2.New()
	var limit int
	limit = int(math.Max(0, float64(maxRows)))
	log.Printf("Marking records for processing with uuid: %s", uuid)
	rows, err := tx.Exec(`
		update gos_tdi_interface set processing_transaction = $1,
		locking_transaction = $2
		where id in (select id from gos_tdi_interface where processing_timestamp is null and locking_transaction is null
		limit $3)`,
		uuid, uuid, limit,
	)
	if err != nil {
		return 0, 0, shared.ErrorArray(err)
	}
	affected, err := rows.RowsAffected()
	if err != nil {
		return 0, 0, shared.ErrorArray(err)
	}
	log.Printf("Marked %d tdi records for acceptance extractions: %s", affected, uuid)
	log.Println(fmt.Sprintf("Extracting data from tdi records and inserting into gos_acceptance_protocol_tdi: %s", uuid))
	rows, err = tx.Exec(`
with current_transaction_acc as (
select ti.id, vc.virtual_card_master_id, ti.json_content->>'LocationId' as location_id, ti.json_content->>'Timestamp' as ts, ti.json_content->>'LocationName' as location_name,
ti.json_content->>'ProductId' as product_id, ti.json_content->>'PermissionId' as permission_id, ti.json_content from gos_tdi_interface ti
join ms_identity.gos_virtual_card vc on vc.virtual_card_type_2_medium_id->'em_dec' = substr(json_content->>'DataCarrierId', 3)
where ti.processing_transaction = $1)
insert into ms_identity.gos_acceptance_protocol_tdi(card_status, acceptance_timestamp, transaction_id, virtual_card_master_id, device_id, attributes, tenant, acceptance_outcome)
select 'accepted', to_timestamp((regexp_match(json_content->>'Timestamp', '^/Date\(([0-9]+).*\)'))[1]::numeric / 1000), $2, virtual_card_master_id, location_id, hstore('tdi_id', acc.id::varchar) || hstore('tdi_json_content', acc.json_content::varchar), 'HTG', 'terminal_accept'
from current_transaction_acc acc`, uuid, time.Now().Unix())
	if err != nil {
		return 0, 0, shared.ErrorArray(err)
	}
	log.Println("Marking tdi records as processed")
	rows, err = tx.Exec(`update gos_tdi_interface set processing_timestamp = current_timestamp, locking_transaction = null where processing_transaction = $1`, uuid)
	if err != nil {
		return 0, 0, shared.ErrorArray(err)
	}
	transactionSuccess = true
	r, _ := rows.RowsAffected()
	log.Printf("Imported %d acceptances", r)
	return r, 0, shared.ErrorsArrayOrNil(rowErrors)
}
