package main

import (
	"acceptance-import/db"
	_ "acceptance-import/db"
	"acceptance-import/shared"
	"acceptance-import/skidata"
	"database/sql"
	"flag"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"os"
	"time"
)

const (
	importedTicketsProcessingLimit = 5000
	archivingLimit                 = 5000
	importFromTdi                  = true
)

func isTodayOrAfter(dt time.Time) bool {
	now := time.Now()
	if dt.After(now) {
		return true
	}
	if dt.Year() == now.Year() && dt.YearDay() == now.YearDay() {
		return true
	}
	return false
}

func main() {
	var configVal string
	flag.StringVar(&configVal, "config", "consolidated", "Set the config. Valid values are demo, temp, localhost, consolidated or gateway.")
	flag.Parse()
	if configVal == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}
	var config skidata.Config
	switch configVal {
	case "demo":
		config = skidata.ConfigDemoSystem
	case "temp":
		config = skidata.ConfigTempSystem
	case "localhost":
		config = skidata.ConfigLocalhostProdTunnel
	case "gateway":
		config = skidata.ConfigEnvServerGatewayMachine
	case "consolidated":
		config = skidata.ConfigConsolidatedSkidataSystem
	default:
		flag.PrintDefaults()
		os.Exit(1)
	}
	defer db.Database.Close()

	var tdiImportDate time.Time
	var acceptanceConsolidationDate time.Time
	for true {
		success := true
		tx, err := db.Database.Begin()
		if err != nil {
			panic(err.Error())
		}
		// Get the tdi upcoming import date
		row := db.Database.QueryRow(fmt.Sprintf(
			"select upcoming_date from gos_tdi_import_date where config_key = '%s' for update", config.ConfigKey))
		var scanned = true
		if row != nil {
			if row.Scan(&tdiImportDate) == sql.ErrNoRows {
				scanned = false
			}
		}
		if !scanned {
			tdiImportDate = time.Date(2018, time.January, 1, 0, 0, 0, 0, time.UTC)
			db.Database.Exec(`insert into gos_tdi_import_date values($1, $2)`, tdiImportDate, config.ConfigKey)
		}
		// Get the
		row = db.Database.QueryRow("select upcoming_date from gos_tdi_acceptance_import_date for update")
		scanned = true
		if row != nil {
			if row.Scan(&acceptanceConsolidationDate) == sql.ErrNoRows {
				scanned = false
			}
		}
		if !scanned {
			acceptanceConsolidationDate = time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC)
			db.Database.Exec(`insert into gos_tdi_acceptance_import_date values($1)`, acceptanceConsolidationDate)

		}
		upperAcceptanceConsolidationDate := tdiImportDate.AddDate(0, 0, -2)
		err, dt := skidata.ConsolidateAcceptances(acceptanceConsolidationDate, upperAcceptanceConsolidationDate)
		if err != nil {
			panic(err.Error())
		}
		db.Database.Exec("update gos_tdi_acceptance_import_date set upcoming_date = $1", dt)

		err = skidata.ArchiveProcessedRecords(archivingLimit)
		if err != nil {
			panic(err.Error())
		}
		err = skidata.MoveTdiToEtl()
		if err != nil {
			panic(err.Error())
		}
		if isTodayOrAfter(tdiImportDate) {
			log.Printf("TDI interface query date %s is today or future. Will not import anything, but will look for unprocessed data.", tdiImportDate.Format(time.RFC822Z))
			_, _, errors := skidata.ProcessImportedTickets(importedTicketsProcessingLimit)
			if errors != nil {
				for _, e := range errors {
					panic(e.Error())
				}
			}
			return
		}
		if importFromTdi {
			errors := skidata.ImportData(config, shared.NewTdiDate(tdiImportDate.Year(), tdiImportDate.Month(), tdiImportDate.Day()))
			if errors != nil {
				for _, e := range errors {
					panic(e.Error())
				}
			}
		}
		_, _, errors := skidata.ProcessImportedTickets(importedTicketsProcessingLimit)
		if errors != nil {
			for _, e := range errors {
				panic(e.Error())
			}
		}
		if importFromTdi {
			tdiImportDate = tdiImportDate.AddDate(0, 0, 1)
			db.Database.Exec("update gos_tdi_import_date set upcoming_date = $1 where config_key = $2", tdiImportDate, config.ConfigKey)
		}
		db.EndTransaction(tx, &success)
	}

}
