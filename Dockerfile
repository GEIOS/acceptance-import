FROM golang:1.12-alpine

ENV CMD_ARGS ""

WORKDIR /go/src/acceptance-import
RUN apk add --update curl git
RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
COPY . .
RUN dep ensure
RUN go build -o acceptance-import acceptance-import/main
ENTRYPOINT ./acceptance-import $CMD_ARGS
