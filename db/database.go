package db

import (
	"database/sql"
	"fmt"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/postgres"
	_ "github.com/golang-migrate/migrate/source/file"
	_ "github.com/lib/pq"
	"log"
	"os"
)

var connStr = os.ExpandEnv("user=$DB_USER password=$DB_PASSWORD dbname=$DB_NAME host=$DB_HOST port=$DB_PORT application_name='tdi-importer'")
var connStrExp = os.ExpandEnv("user=$DB_USER password=****** dbname=$DB_NAME host=$DB_HOST port=$DB_PORT application_name='tdi-importer'")
var Database, databaseError = sql.Open("postgres", connStr)

func init() {
	log.Printf("Starting database setup: %s", connStrExp)
	if databaseError != nil {
		panic(databaseError)
	}
	config := postgres.Config{}
	config.MigrationsTable = "schema_migration_tdi_interface"
	driver, err := postgres.WithInstance(Database, &config)
	if err != nil {
		panic(err)
	}
	dir, err := os.Getwd()
	d := fmt.Sprintf("file://%s/migrations", dir)
	log.Printf("Starting db migration at %s", d)
	m, err := migrate.NewWithDatabaseInstance(d, "postgres", driver)
	if err != nil {
		panic(err)
	}
	defer m.Close()
	err = m.Up()
	if err != nil {
		log.Printf("Please carefully analyse this message, could be an migration error: %s", err)
	}
}

func EndTransaction(transaction *sql.Tx, success *bool) {
	if *success {
		log.Println("Comitting transaction")
		transaction.Commit()
	} else {
		log.Println("Rolling back transaction")
		transaction.Rollback()
	}
}
