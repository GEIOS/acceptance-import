package shared

import (
	"fmt"
	"time"
)

type TdiDate struct {
	time.Time
}

func (dt TdiDate) ToTdiDateString() string {
	return fmt.Sprintf("/Date(%d)/", dt.Unix()*1000)
}

func (dt TdiDate) NextDate() TdiDate {
	return TdiDate{dt.AddDate(0, 0, 1)}
}

func NewTdiDateFromEpoch(epoch int64) TdiDate {
	return TdiDate{time.Unix(epoch/1000, 0)}
}

func NewTdiDate(year int, month time.Month, day int) TdiDate {
	return TdiDate{time.Date(year, month, day, 0, 0, 0, 0, time.UTC)}
}
