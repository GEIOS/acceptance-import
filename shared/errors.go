package shared

import (
	"fmt"
	"runtime/debug"
)

func ErrorArray(e error) []error {
	return []error{Error(e)}
}

func Error(e error) error {
	return fmt.Errorf("%s - %s", e, debug.Stack())
}

func ErrorToArray(e error, existing []error) []error {
	return append(existing, Error(e))
}

func ErrorsArrayOrNil(e []error) []error {
	if e == nil || len(e) == 0 {
		return nil
	}
	return e
}